# HELICS conda
[![](https://anaconda.org/kdheepak/helics/badges/platforms.svg)](https://anaconda.org/kdheepak/helics) [![](https://anaconda.org/kdheepak/helics/badges/latest_release_date.svg)](https://anaconda.org/kdheepak/helics) [![](https://anaconda.org/kdheepak/helics/badges/downloads.svg)](https://anaconda.org/kdheepak/helics) [![Build Status](https://www.travis-ci.org/kdheepak/helics-conda.svg?branch=master)](https://www.travis-ci.org/kdheepak/helics-conda) [![AppVeyor](https://img.shields.io/appveyor/ci/kdheepak/helics-conda.svg)](https://ci.appveyor.com/project/kdheepak/helics-conda)

See [docs](https://gmlc-tdc.github.io/HELICS-src) for more information.
